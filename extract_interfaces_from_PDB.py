from sys import path, argv
path.append('.')
from biopandas.pdb import PandasPdb
from os.path import basename, splitext
import pandas as pd
import numpy as np
import itertools
import math
from scipy.spatial.distance import cdist

#simple routine that uses scipy's cdist to calculate all distances between two sets of atoms
def cmap(atoms1, atoms2, threshold='inf', form='binary'):
        
    dist = cdist(atoms1, atoms2)
        
    return dist

input_molecule = argv[1]
threshold = float(argv[2])

output_molecule = 'interfaces/' + splitext(basename(input_molecule))[0] + '_'

molecule = PandasPdb().read_pdb(input_molecule)

#we want to extract the interfaces from
#either side of the interaction
#in a new molecule
molecule12 = PandasPdb()
molecule21 = PandasPdb()

#all the models and chains in the biological unit
models = molecule.df['ATOM']['model_number'].unique()
chains = molecule.df['ATOM']['chain_id'].unique()

#but we only do the first model against all the others
#because of the way biological units contain models
#that are identical, just expanded simmetrically
#hence we should have the same interfaces on all models
models1 = [models[0]]

for model1 in models1:
    for chain1 in chains:

        model1_chain1 = molecule.df['ATOM'][(molecule.df['ATOM']['chain_id'] == chain1)
                                                           & (molecule.df['ATOM']['model_number'] == model1)
                                                           & (molecule.df['ATOM']['alt_loc'].values == '')]
        atoms1 = model1_chain1[['x_coord', 'y_coord', 'z_coord']]
        
        for model2 in models:

            for chain2 in chains:

                if model1 <= model2 and chain1 <= chain2 and (model1 != model2 or chain1 != chain2):

                    model2_chain2 = molecule.df['ATOM'][(molecule.df['ATOM']['chain_id'] == chain2) 
                                  & (molecule.df['ATOM']['model_number'] == model2)
                                  & (molecule.df['ATOM']['alt_loc'].values == '')]

                    atoms2 = model2_chain2[['x_coord', 'y_coord', 'z_coord']]

                    contact_map = cmap(atoms1, atoms2)
                    
                    contacts = np.argwhere(contact_map < threshold) 
                    if contacts.size >= 5:
                        
                        contact_residues12 = model1_chain1.iloc[contacts[:,0]]['residue_number']
                        contact_residues21 = model2_chain2.iloc[contacts[:,1]]['residue_number']
                                               
                        molecule12.df['ATOM'] = model1_chain1[model1_chain1['atom_name'] == 'CA'][model1_chain1['residue_number'].isin(contact_residues12)]
                        molecule21.df['ATOM'] = model2_chain2[model2_chain2['atom_name'] == 'CA'][model2_chain2['residue_number'].isin(contact_residues21)]

                        if molecule12.df['ATOM'].size > 0:                        
	                        molecule12.to_pdb(path=output_molecule + str(model1) + chain1 + str(model2) + chain2 +'.pdb', records=['ATOM'], gz=False, append_newline=True)
                        if molecule21.df['ATOM'].size > 0:
                        	molecule21.to_pdb(path=output_molecule + str(model2) + chain2 + str(model1) + chain1 +'.pdb', records=['ATOM'], gz=False, append_newline=True)
                
