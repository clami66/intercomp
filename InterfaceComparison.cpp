/*
 * InterfaceComparison.cpp
 *
 *  Created on: Feb 10, 2016
 *      Author: Claudio Mirabello <claudio.mirabello@liu.se>
 */

#include "InterfaceComparison.h"
#include <fstream>
#include <sstream>
#include <vector>
#include <iostream>

#define FASTRAND_MAX 32767.00

#include <stdio.h>  /* defines FILENAME_MAX */
#include <unistd.h>

using namespace std;

bool verbose = false;
bool superimpose = false;
bool apply = false;
bool map = false;

double best = -1;

int rounds = 1;
double chainMult = 10.0;

double percent = 0;
double T0 = 0.2;
double d0 = 0.5;
double epsilon = 0.5;
double dW = 0.5; //DEFAULT VALUES
long double pvalues[30][1001];
long double pvaluesMol2[36][1001];

char to_apply[200];
std::string blosum;


int nulls;
RunningStat rs;

Molecule* molA;
Molecule* molB;
InterfaceComparison* compare;

static unsigned int g_seed;
int seed = 0;
//srand (time(NULL));

//Used to seed the generator.
inline void fast_srand(int seed) {

	g_seed = seed;

}

//fastrand routine returns one integer, similar output value range as C lib.
inline int fastrand() {

	g_seed = (214013 * g_seed + 2531011);

	return (g_seed >> 16) & 0x7FFF;

}

std::string GetCurrentWorkingDir(){

	//char buff[FILENAME_MAX];
	//GetCurrentDir(buff, FILENAME_MAX);
	//std::string current_working_dir(buff);

	std::string path = "";
	pid_t pid = getpid();
	char buf[20] = {0};
	sprintf(buf,"%d",pid);

	std::string _link = "/proc/";
	_link.append( buf );
	_link.append( "/exe");
	char buff[1024];

	int ch = readlink(_link.c_str(), buff, 1024);

	if (ch != -1) {
		buff[ch] = 0;
		path = buff;
		std::string::size_type t = path.find_last_of("/");
		path = path.substr(0, t);
	}

	return path;
}

void p_values() {

	std::ifstream file((GetCurrentWorkingDir() + "/p_values_intsize_shellsize_step5_0.5_0.5_0.5").c_str());

	int intl = 0;

	for (int row = 0; row < 30; row++) {
		std::string line;
		std::getline(file, line);
		if (!file.good())
			break;

		std::stringstream iss(line);

		for (int col = 0; col < 1002; ++col) {

			std::string val = "";
			std::getline(iss, val, ' ');
			if (!iss.good())
				break;
			std::stringstream convertor(val);

			if (col == 0) {
				convertor >> intl;
			} else {
				convertor >> pvalues[int(intl / 5)][col - 1];
			}
		}
	}
	return;
}

void p_values_mol2() {

	std::ifstream file((GetCurrentWorkingDir() + "//p_values_mol2size_step1_0.6_0.25_0.75").c_str());

	int intl = 0;

	for (int row = 0; row < 36; row++) {
		std::string line;
		std::getline(file, line);
		if (!file.good())
			break;

		std::stringstream iss(line);

		for (int col = 0; col < 1002; ++col) {

			std::string val = "";
			std::getline(iss, val, ' ');
			if (!iss.good())
				break;
			std::stringstream convertor(val);

			if (col == 0) {
				convertor >> intl;
			} else {
				convertor >> pvaluesMol2[int(intl) - 5][col - 1];
			}
		}
	}
	return;
}

std::vector<Molecule*> read_list_of_pdbs(char* list){

	std::vector<Molecule*> molecules;

	std::string line;
	std::ifstream infile(list);
	int counter = 0;
	while (std::getline(infile, line)){
		char* filename = const_cast<char*>(line.c_str());
		Molecule* thisMol = new Molecule(filename, "PDB");
		molecules.push_back(thisMol);

		if(counter % 10000 == 0 && verbose){
			cout << "Loading molecules: " << counter << "\n" << flush;
		}
		counter++;
	}

	return molecules;

}

std::vector<Molecule*> read_list_of_mol2s(char* file){

	std::vector<Molecule*> molecules;

	ifstream fin(file); //to count stuff
	ifstream fin2(file);	//to read stuff

	string line;
	string molecule("@<TRIPOS>MOLECULE");

	int nMolecules = 0;

	getline(fin, line);

	while (fin.good()) {

		if (line.compare(0, molecule.length(), molecule) == 0)
			nMolecules++;

		getline(fin, line);
	}

	fin.clear();
	fin.seekg(0, std::ios::beg);


	for (int i = 0; i < nMolecules; i++) {

		Molecule* thisMol = new Molecule(&fin, &fin2);

		molecules.push_back(thisMol);

		if(i % 10000 == 0 && verbose){
			cout << "Loading molecules: " << i << "\n" << flush;
		}
	}

	return molecules;

}

void parse_options(int argc, char* argv[]){

	for (int arg = 4; arg < argc; arg++){

		if (!strcmp(argv[arg], "-nullP")){

			percent = atoi(argv[arg + 1]);

		}else if (!strcmp(argv[arg], "-anneal")){

			rounds = atoi(argv[arg + 1]);
			if (verbose)
				cout << "Annealing rounds: " << rounds << "\n";

		} else if (!strcmp(argv[arg], "-d0")){

			d0 = atof(argv[arg + 1]);

			if (verbose)
				cout << "d0: " << d0 << "\n";

		}else if(!strcmp(argv[arg], "-v")){

			cout << "Verbose" << "\n" << flush;
			verbose = true;

		}else if(!strcmp(argv[arg], "-super")){

			cout << "Superimpose" << "\n" << flush;
			superimpose = true;

		}else if(!strcmp(argv[arg], "-apply")){

			apply = true;

			strcpy(to_apply, argv[arg + 1]);

			if(verbose)
				cout << "Apply to third molecule: " << to_apply << "\n" << flush;

		}else if(!strcmp(argv[arg], "-eps")){

			epsilon = atof(argv[arg + 1]);
			if (verbose)
				cout << "Epsilon: " << epsilon << "\n" << flush;

		}else if(!strcmp(argv[arg], "-matrix")){

			blosum = argv[arg + 1];

			if (verbose)
				cout << "Blosum matrix: " << blosum << "\n" << flush;

		}else if(!strcmp(argv[arg], "-dW")){

			dW = atof(argv[arg + 1]);

			if (verbose)
				cout << "Sequence weight: " << dW << "\n" << flush;

		}else if(!strcmp(argv[arg], "-seed")){

			seed = atoi(argv[arg + 1]);

			if (verbose)
				cout << "New seed: " << atoi(argv[arg + 1]) << "\n" << flush;

		}else if(!strcmp(argv[arg], "-ch")){

			chainMult = atof(argv[arg + 1]);
			if (verbose)
				cout << "New chainMult: " << atof(argv[arg + 1]) << "\n" << flush;

		}else if(!strcmp(argv[arg], "-T0")){

			T0 = atof(argv[arg + 1]);
			if (verbose)
				cout << "New initial Temperature: " << atof(argv[arg + 1]) << "\n" << flush;

		}else if(!strcmp(argv[arg], "-map")){

			map = true;
			if (verbose)
				cout << "Output mapping of residues\n" << flush;

		}

	}
}

double anneal() {

	//shuffle B, reset null correspondences
	//molB->shuffle(1000);
	molA->resetNull();

	//calculate the difference between the two (only where A and B overlap and A has no null correspondences)

	//rs.Clear();
	int r;
	int c;

	int loop = 1;

	double p = 0;
	double de = 0;


	double T = T0;

	double dice = 0;

	double mean = 0;
	double stdev = 0;

	double accRate = 1;
	int accepted = 1;
	int rejected = 0;

	int maxChainLength = max(400,
			int(chainMult * molB->getLength() * (molB->getLength() - 1))); //2*lengthMin*(lengthMin-1);
	int maxAccept = molB->getLength() * (molB->getLength() - 1);

	int toNull = -1;

	for (int i = 0; i < nulls; i++) {

		while (toNull < 0 || molA->getNull()[toNull]) {
			toNull = fastrand() % molA->getLength();
		}

		molA->setNull(toNull);
	}
	if (verbose)
		cout << "Caching difference..." << "\n" << flush;
	compare->cacheDifference();
	best = compare->getScore();

	if (verbose)
		cout << "Difference cached" << "\n" << flush;

	//a or b selection parameters
	char AorB = 'A';

	while (1) {

		if (nulls == 0 || loop % 3) {

			//swapping on B
			AorB = 'B';

			r = 0;
			c = 0;

			/*illegal moves are:				(for c, r in [0; lengthB), c > r)
			 1) swap two residues aligned to null correspondences in A (nullA[c] and nullA[r])
			 2) swap a residue in a null correspondence in A with a residue in the outer subset in B (nullA[r] and c >= lengthA)
			 3) swap two residues in the outer subset of B (r >= lengthA, c will be too then)
			 */
			while (r >= c || (molA->getNull()[c] and molA->getNull()[r])
					|| (molA->getNull()[r] and c >= molA->getLength())
					|| r >= molA->getLength()) {

				r = fastrand() % molB->getLength();
				c = fastrand() % molB->getLength();

			}

		} else {
			//null correspondences on A

			AorB = 'A';

			r = 0;
			c = 0;

			/*illegal moves are:				(for c, r in [0; lengthA), c > r)
			 1) swap two residues aligned to null correspondences in A (nullA[c] and nullA[r])
			 2) swap two non null residues
			 */
			while (r >= c || (molA->getNull()[c] and molA->getNull()[r])
					|| (!(molA->getNull()[c]) and !(molA->getNull()[r]))) {
				r = fastrand() % molA->getLength();
				c = fastrand() % molA->getLength();
			}

		}

		if (AorB == 'A') {

			compare->deltaA(r, c);
			compare->deltaSeqA(r, c);
		}

		if (AorB == 'B') {

			compare->deltaB(r, c);
			compare->deltaSeqB(r, c);

		}

		de = epsilon * -compare->getDeltaN()
										+ dW * (double) compare->getDeltaS();// / compare->getDeltaD();
		rs.Push(compare->getScore() + de);

		if (accepted > maxAccept || accepted + rejected > maxChainLength) {

			//starting a new markov chain
			mean = rs.Mean();
			stdev = rs.StandardDeviation();

			T = compare->temperatureSD(mean, stdev, T);

			accepted = 1;
			rejected = 0;

			rs.Clear();
		}

		if (de < 0) {

			p = 1;

		} else {

			p = exp(-de / T);

		}

		dice = (((double) fastrand() / (FASTRAND_MAX)));

		if (p > dice) {	//probability

			accepted++;

			if (AorB == 'A') {

				molA->swapA(r, c);
				compare->updateDifferenceA2(r, c);

			}

			if (AorB == 'B') {

				molB->swapB(r, c);
				compare->updateDifferenceB2(r, c);
			}

			if (compare->getScore() > best) {

				best = compare->getScore();

				/*if (verbose)
					cout << "New best: " << best << " deltaN: "
					<< -compare->getDeltaN() << " deltaS: "
					<< compare->getDeltaS() << "\n";*/

			}

		} else {

			rejected++;

		}

		accRate = (double) accepted / (double) rejected;

		if (accRate < 0.003) {

			if (verbose) {
				molA->printSeq(molA->getLength());
				molB->printSeq(molB->getLength());
			}

			break;

		}
		loop++;
	}

	return best;
}

void run_pdb(int argc, char* argv[]) {

	p_values(); //load the pvalues from file into array
	blosum = GetCurrentWorkingDir() + "/BLOSUM62";

	srand(seed);
	fast_srand(seed);

	Molecule* mol1 = new Molecule(argv[2], "PDB");
	mol1->cacheDist();
	Molecule* mol2 = new Molecule(argv[3], "PDB");
	mol2->cacheDist();

	int length1 = mol1->getLength();
	int length2 = mol2->getLength();

	nulls = round(double(min(length1, length2)) / 100.0 * percent);
	if (verbose)
		cout << "Max nulls: " << nulls << "\n";

	if (length1 > length2) {

		molA = mol2;
		molB = mol1;

	} else {

		molA = mol1;
		molB = mol2;

	}

	molB->shuffle();

	if (verbose) {

		cout << "Mol A length:" << " " << molA->getLength() << "\n" << flush;
		cout << "Mol B length:" << " " << molB->getLength() << "\n" << flush;

	}

	rs.Clear();

	if (verbose)
		cout << "Creating InterFaceComparison object..." << "\n";

	compare = new InterfaceComparison(molA, molB);
	compare->loadBLOSUM62(blosum.c_str());
	compare->setD0(d0);

	if (verbose)
		cout << "Done." << "\n";

	for (int r = 0; r < rounds; r++) {

		anneal();

	}

	int pvalue_xindex = int(molA->getLength() / 5);

	pvalue_xindex = min(pvalue_xindex, 29);

	int pvalue_zindex = round(best * 1000);

	double best_pvalue = pvalues[pvalue_xindex][pvalue_zindex];

	if(map){
		cout << "Mapping of residue numbers: (Input 1 -> Input 2)\n";
		for(int i = 0; i < molA->getLength(); i++){
			cout << mol1->getResN(i) << " -> " << mol2->getResN(i) << "\n";
		}
	}

	cout << "Best" << ": " << best << " sequence score: "
			<< compare->getsScore() << " length A: "
			<< molA->getLength() - nulls << " length B: " << molB->getLength()
			<< flush;

	if (d0 == 0.5 && dW == 0.5 && epsilon == 0.5) {
		cout << " p-value: " << best_pvalue << "\n" << flush;
	} else {
		cout << "\n" << flush;
	}

	best = 0;

	if (superimpose) {

		FILE *fp1, *fp2, *fp3;

		std::string base_filenameA = molA->getFileName().substr(molA->getFileName().find_last_of("/\\") + 1);
		std::string base_filenameB = molB->getFileName().substr(molB->getFileName().find_last_of("/\\") + 1);

		std::string filename_superA = base_filenameA + "_" + base_filenameB;
		std::string filename_superB = base_filenameB + "_" + base_filenameA;

		fp1 = fopen(filename_superA.c_str(), "w+");
		fp2 = fopen(filename_superB.c_str(), "w+");

		double rmsd = compare->superimpose(molA, molB);
        cout << "Superposition error: " << rmsd << "\n";

		if (apply) {

			Molecule* mol3 = new Molecule(to_apply, "fullPDB");
			std::string base_filenameC = mol3->getFileName().substr(mol3->getFileName().find_last_of("/\\") + 1);
			std::string filename_superC = base_filenameC + "_" + base_filenameA;

			fp3 = fopen(filename_superC.c_str(), "w+");

			double* firstTrans = compare->getTranslation2();

			for (int i = 0; i < 3; i++) {
				firstTrans[i] = -firstTrans[i];
			}

			mol3->rototranslate(compare->getI(), firstTrans);
			mol3->rototranslate(compare->getRotation(),
					compare->getTranslation1());
			mol3->printPDB(fp3, mol3->getLength(), "fullPDB");
			molA->printPDB(fp1, molA->getLength(), "PDB",
					compare->getDiffCumulative());
			molB->printPDB(fp2, molA->getLength(), "PDB",
					compare->getDiffCumulative());
		} else {
			molA->printPDB(fp1, molA->getLength(), "PDB");
			molB->printPDB(fp2, molA->getLength(), "PDB");
		}

		fclose(fp1);
		fclose(fp2);

	}


	mol1->destroy();
	mol2->destroy();

}

void run_pdb_list(int argc, char* argv[]) {

	blosum = GetCurrentWorkingDir() + "/BLOSUM62";
	p_values(); //load the pvalues from file into array

	Molecule* mol1 = new Molecule(argv[2], "PDB");
	mol1->cacheDist();
	int length1 = mol1->getLength();
	int molN = 0;

	std::vector<Molecule*> mols2 = read_list_of_pdbs(argv[3]);
	for(std::vector<Molecule*>::iterator mol2 = mols2.begin(); mol2 != mols2.end(); ++mol2){

		srand(seed);
		fast_srand(seed);
		(*mol2)->cacheDist();

		//Molecule* mol2 = new Molecule(argv[3], "PDB");
		int length2 = (*mol2)->getLength();

		nulls = round(double(min(length1, length2)) / 100.0 * percent);

		if (length1 > length2) {

			molA = (*mol2);
			molB = mol1;

		} else {

			molA = mol1;
			molB = (*mol2);
			//molB->shuffle();

		}



		//load PDBs
		if (verbose) {

			cout << "Mol A length:" << " " << molA->getLength() << "\n" << flush;
			cout << "Mol B length:" << " " << molB->getLength() << "\n" << flush;

		}

		rs.Clear();

		if (verbose)
			cout << "Creating InterFaceComparison object..." << "\n";

		if(molN == 0){
			compare = new InterfaceComparison(molA, molB);
			compare->loadBLOSUM62(blosum.c_str());
			compare->setD0(d0);
			compare->initializeLgmap();
		}else{
			compare->reset(molA, molB);
			compare->setD0(d0);
		}
		molN++;

		if (verbose)
			cout << "Done." << "\n";

		for (int r = 0; r < rounds; r++) {

			anneal();

		}

		int pvalue_xindex = int(molA->getLength() / 5);

		pvalue_xindex = min(pvalue_xindex, 29);

		int pvalue_zindex = round(best * 1000);

		double best_pvalue = pvalues[pvalue_xindex][pvalue_zindex];

		std::string base_filename1 = mol1->getFileName().substr(mol1->getFileName().find_last_of("/\\") + 1);
		std::string base_filename2 = (*mol2)->getFileName().substr((*mol2)->getFileName().find_last_of("/\\") + 1);

		cout << base_filename1 << " " << base_filename2 << " Best" << ": " << best << " sequence score: "
				<< compare->getsScore() << " length 1: "
				<< mol1->getLength() << " length 2: " << (*mol2)->getLength()
				<< flush;

		if (d0 == 0.5 && dW == 0.5 && epsilon == 0.5) {
			cout << " p-value: " << best_pvalue << "\n" << flush;
		} else {
			cout << "\n" << flush;
		}

		best = 0;

		if (superimpose) {

			FILE *fp1, *fp2, *fp3;
			fp1 = fopen("molA.pdb", "w+");
			fp2 = fopen("molB.pdb", "w+");

			compare->superimpose(molA, molB);

			if (apply) {

				Molecule* mol3 = new Molecule(to_apply, "fullPDB");
				fp3 = fopen("molC.pdb", "w+");
				//mol3->center_molecule(molA->getLength());
				double* firstTrans = compare->getTranslation2();

				for (int i = 0; i < 3; i++) {
					firstTrans[i] = -firstTrans[i];
				}

				mol3->rototranslate(compare->getI(), firstTrans);
				mol3->rototranslate(compare->getRotation(),
						compare->getTranslation1());
				mol3->printPDB(fp3, mol3->getLength(), "fullPDB");
				molA->printPDB(fp1, molA->getLength(), "PDB",
						compare->getDiffCumulative());
				molB->printPDB(fp2, molA->getLength(), "PDB",
						compare->getDiffCumulative());
			} else {
				molA->printPDB(fp1, molA->getLength(), "PDB");
				molB->printPDB(fp2, molA->getLength(), "PDB");
			}

			fclose(fp1);
			fclose(fp2);

		}

		(*mol2)->destroy();
	}
}

void run_mol(int argc, char* argv[]) {

	blosum = GetCurrentWorkingDir() + "/MOL2";
	p_values_mol2();
	//according to algorithm with null correspondences, B molecule is always the largest of the two
	Molecule* mol1 = new Molecule(argv[2], "mol");
	mol1->cacheDist();
	int length1, length2;

	length1 = mol1->getLength();

	if (verbose)
		cout << "reading molecules...\n";

	int molN = 0;
	std::vector<Molecule*> mols2 = read_list_of_mol2s(argv[3]);

	for(std::vector<Molecule*>::iterator mol2 = mols2.begin(); mol2 != mols2.end(); ++mol2){

		srand(seed);
		fast_srand(seed);
		(*mol2)->cacheDist();

		length2 = (*mol2)->getLength();

		nulls = round(double(min(length1, length2)) / 100.0 * percent);

		if (verbose)
			cout << "Nulls: " << nulls << "\n";

		if (length1 > length2) {

			molA = (*mol2);
			molB = mol1;

		} else {

			molA = mol1;
			molB = (*mol2);

		}

		//molB->shuffle();
		if (verbose) {

			cout << "Molecule A length: " << molA->getLength() << "\n";
			cout << "Molecule B length: " << molB->getLength() << "\n";

		}

		rs.Clear();




		if(molN == 0){
			if (verbose)
				cout << "Creating InterFaceComparison object..." << "\n" << flush;

			compare = new InterfaceComparison(molA, molB);
			compare->loadMOL2(blosum.c_str());
			compare->initializeLgmap();
			compare->setD0(d0);

		}else{

			if(verbose)
				cout << "Resetting InterfaceComparison object...\n" << flush;
			compare->reset(molA, molB);
			compare->setD0(d0);
		}
		molN++;

		if (verbose)
			cout << "Done." << "\n";

		for (int r = 0; r < rounds; r++) {

			anneal();

		}
		cout << "Best for molecule " << (*mol2)->getName() << ": " << best << " sequence score: "
				<< compare->getsScore() << " length 1: "
				<< mol1->getLength() << " length 2: " << (*mol2)->getLength()
				<< flush;

		int pvalue_xindex = int(molA->getLength() - 5);

		pvalue_xindex = min(pvalue_xindex, 35);
		pvalue_xindex = max(pvalue_xindex, 0);

		int pvalue_zindex = round(best * 1000);

		double best_pvalue = pvaluesMol2[pvalue_xindex][pvalue_zindex];

		//if (d0 == 0.6 && dW == 0.75 && epsilon == 0.25) {
			cout << " p-value: " << best_pvalue << "\n" << flush;
		//} else {
		//	cout << "\n" << flush;
		//}

		best = 0;
		(*mol2)->destroy();

	}

}

int main(int argc, char *argv[]) {

	fast_srand(time(NULL));
	srand(time(NULL));

	if(argc == 1){
		cout << "Usage: ./InterfaceComparison [-PDB, -list, -mol2] target1 target2 <options>\n"
				<< "\nFor more help: InterfaceComparison -h\n";
		return EXIT_FAILURE;
	}
	if (!strcmp(argv[1], "-h")) {

		cout << "\nUsage: ./InterfaceComparison [-PDB, -list, -mol2] target1 target2 <options>\n\n" <<
				"   -PDB   : Compare two PDB files\n" <<
				"          (./InterfaceComparison -PDB examples/target1.pdb examples/target2.pdb)\n" <<
				"   -list  : Compare a PDB file with a list of PDB files (one path per line)\n" <<
				"          (./InterfaceComparison -list examples/target1.pdb examples/pdb_list)\n" <<
				"   -mol2  : Compare a mol2 file with a mol2 database (multiple molecules in the database)\n" <<
				"          (./InterfaceComparison -mol2 examples/target.mol2 examples/database.mol2)\n" <<

				"\nGeneral options:\n\n" <<

				"   -v             : verbose output\n" <<
				"   -h             : print this help\n" <<
				"   -matrix <path> : optional path to the sequence similarity matrix (default: <current_path>/BLOSUM62)\n"  <<

				"\nAlignment options:\n\n" <<

				"   -d0            : d0 parameter in the Levitt-Gerstein score (default: 0.5)\n" <<
				"   -dW            : weight of sequence similarity (e.g. BLOSUM62) in the optimization procedure (default: 0.5)\n" <<
				"   -eps           : weight of structural similarity in the optimization procedure (default: 0.5)\n" <<
				"   -nullP         : percentage of ignored atoms in the smaller molecule (default: 0)\n" <<
				"   -super         : calculate optimal superposition between two aligned PDB files (only -PDB mode)\n" <<
				"                    (output to molA.pdb and molB.pdb files)\n" <<
				"   -apply target3 : apply the same superposition as in -super to a third molecule target3 (only -PDB mode)\n" <<
				"                    (output to molC.pdb file)\n" <<

				"\nAnnealing options:\n\n" <<
				"   -anneal <int>  : number of annealing rounds (default: 1)\n" <<
				"   -ch <int>      : Markov chain length multiplier (default: 10)\n" <<
				"   -T0 <float>    : Initial annealing temperature (default: 0.2)\n" <<
				"   -seed <int>    : seed for the stochastic process (random number generation)\n";

	}else if(!strcmp(argv[1], "-PDB")) {

		parse_options(argc, argv);
		run_pdb(argc, argv);

	}else if (!strcmp(argv[1], "-list")) {

		parse_options(argc, argv);
		run_pdb_list(argc, argv);

	} else if (!strcmp(argv[1], "-mol2")) {

		parse_options(argc, argv);
		run_mol(argc, argv);

	} else {

		cout << "Usage: ./InterfaceComparison [-PDB, -list, -mol2] target1 target2 <options>\n"
				<< "For more help: InterfaceComparison -h\n";
		return EXIT_FAILURE;
	}

}

